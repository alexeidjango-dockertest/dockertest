# base image for uptime

FROM alpine:latest

RUN apk --no-cache add g++ python3 python3-dev python2 python2-dev py2-pip py2-virtualenv postgresql postgresql-dev

#RUN mkdir -p /test_app

#COPY ./requirements.txt requirements.txt
#COPY ./tox.ini tox.ini

RUN python -m pip install tox pylint

#ENV VIRTUAL_ENV=/venv
#RUN virtualenv --python=python2.7 $VIRTUAL_ENV
#ENV PATH="$VIRTUAL_ENV/bin:$PATH"

#RUN ./venv/bin/pip install -r ./requirements.txt
#COPY ./test_app ./test_app

 
