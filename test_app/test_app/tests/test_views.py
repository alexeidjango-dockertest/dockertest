from __future__ import absolute_import, unicode_literals

from unittest import skip

from django.shortcuts import reverse
from django.test import TestCase


class TestAppViewsTestViewTestCase(TestCase):
    def test_render(self):
        response = self.client.get(reverse('test_view'))
        self.assertContains(response, 'Hello There')
